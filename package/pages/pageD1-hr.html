<p>Poluvodička dioda je elektronička komponenta s dva konektora (slično kao otpornik) koja struju propušta u jednom smjeru ali je ne propušta u drugom. Simbol diode prikazan je na slici u nastavku.</p>

<figure class="mlFigure">
<img src="@#file#(dioda.svg)" alt="Poluvodička dioda: simbol."><br>
<figcaption><span class="mlFigKey">Slika 1.</span> Simbol poluvodičke diode.</figcaption>
</figure>

<p>Terminali diode označeni su slovima A i K.</p> 

<h3>Propusno polarizirana dioda</h3>

<p>Ako je potencijal konektora A (oznaka \(\phi _A\)) viši od potencijala konektora B  (oznaka \(\phi _B\)),
   tada na diodi vlada napon:
   $$
     U_{AB} = \phi _A - \phi _B  \quad &gt; 0.
   $$
   Za taj slučaj kažemo da je dioda <em>propusno polarizirana</em> (vidi sliku 2). Ako je napon \(U_{AB}\) malen (manji od napona praga koji ćemo označiti s \(U_T\)),
   poluvodička dioda se ponaša kao izuzetno velik otpor (reći ćemo: beskonačan otpor) pa struja kroz diodu ne teče.
</p>

<p>Jednom kada napon \(U_{AB}\) dosegne napon praga, dolazi do rapidnog smanjenja otpora diode pa kroz diodu počinje teći struja. Porast struje u odnosu
   na porast napona od tog je trenutka eksponencijalan pa dioda već pri naponima koji su malo veći od napona praga može propuštati vrlo velike struje.
   Zbog činjenice da je otpor diode funkcija napona koji vlada na diodi, dioda spada u takozvane nelinearne elemente.
</p>   

<figure class="mlFigure">
<img src="@#file#(dioda2.svg)" alt="Poluvodička dioda: propusno polarizirana."><br>
<figcaption><span class="mlFigKey">Slika 2.</span> Propusno polarizirana poluvodička dioda.</figcaption>
</figure>

<p>U strujnom krugu, poluvodičku ćemo diodu najčešće spajati u seriju s otpornikom. Takav scenarij prikazan je na
slici u nastavku.</p>

<figure class="mlFigure">
<img src="@#file#(shema-dioda1.png)" alt="Poluvodička dioda: propusno polarizirana.">
<figcaption><span class="mlFigKey">Slika 3.</span> Propusno polarizirana poluvodička dioda u strujnom krugu.</figcaption>
</figure>

<div class="mlNote"><p>Interaktivna simulacija sheme s prethodne slike dostupna je kao Java aplikacija koju možete pokrenuti
uporabom tehnologije Java WebStart. <a href="@#file#(RunD0.jnlp)">(Aplikacija)</a></p>
</div>

<p>Na prikazanoj slici na naponski izvor \(U_{ul}\) serijski su spojeni otpornik iznosa 330&#x2126; i dioda čiji je napon praga \(U_T = 0,7V\).
   Slika prikazuje i tri dijagrama: kretanje napona na diodi, napona na otporniku i struje kroz otpornik, sve u ovisnosti o naponu izvora.</p>

<p>Pokrenite interaktivnu simulaciju ovog strujnog kruga i mijenjajte napon izvora povlačenjem kliznika prikazanog u gornjem lijevom uglu prozora.</p>

<p>Prema drugom Kirchoffovom zakonu, u ovom strujnom krugu mora vrijediti da se sav napon izvora distribuira između otpornika i diode, odnosno vrijedi:
$$
  U_{ul} = U_R + U_D.
$$
Kako napon izvora polako povećavate od 0V na više, sve dok je \(U_{ul} &lt; U_T\), dioda, iako propusno polarizirana, još uvijek ne vodi i predstavlja beskonačan
otpor. Zbog toga struja ne teče pa je prema Ohmovom zakonu \(U_R = I \cdot R = 0 \cdot 330 = 0V\), pa je \(U_D = U_{ul}-U_R = U_{ul}-0 = U_{ul}\). Sav se porast
napona izvora prenosi izravno na diodu; napon na otporniku je 0V i struja ne teče. Uočite taj dio na prikazanim dijagramima.
</p>

<p>Jednom kada napon izvora dosegne napon praga \(U_T\) što je u ovom primjeru 0,7V, dioda počinje voditi te se njezin otpor smanjuje zbog čega počinje teči
struja. Zahvaljujući nelinearnoj karakteristici diode, na ovom mjestu možemo uvesti jedno pojednostavljenje koje će nam biti dovoljno da bismo dobili osjećaj
za ponašanje ovog strujnog kruga: pretpostavit ćemo da će napon koji se održava na diodi ostati na vrijednosti \(U_T\), odnosno da će se sav napon izvora koji je
iznad \(U_T\) trošiti na otporniku. U tom slučaju, kad god je \(U_{ul} &gt; U_T\) vrijedit će:
$$
  U_R = U_{ul} - U_T.
$$
No pogledajte što se sada događa: napon na otporniku raste zajedno s naponom izvora (pomaknut za \(U_T\)) a prema Ohmovom zakonu,
na isti način se u strujnom krugu mijenja i struja. Naime, struju u ovom strujnom krugu možemo napisati na tri osnovna načina:
$$
 I = \frac{U_{ul}}{R_{\text{ukupno}}} = \frac{U_D}{R_D} = \frac{U_R}{R}
$$
od čega nam je najkorisniji posljednji, jer ne znamo koliki je otpor diode a time niti ukupni otpor serije. Kako napon na otporniku znamo izračunati, možemo pisati:
$$
 I = \frac{U_{ul} - U_T}{R}.
$$
I upravo iz ovog razloga smo prethodno rekli da se otpornik koristi za ograničevanje (ili bolje rečeno - namještanje) struje: ako znamo koliki će biti napon izvora
i ako znamo koliku struju želimo u strujnom krugu, možemo odabrati otpor koji treba serijski spojiti da bismo to ostvarili.
</p>

<h3>Nepropusno polarizirana dioda</h3>

<p>Ako je potencijal konektora A niži od potencijala konektora B,
   tada na diodi vlada napon:
   $$
     U_{AB} = \phi _A - \phi _B  \quad &lt; 0.
   $$
   Za taj slučaj kažemo da je dioda <em>nepropusno polarizirana</em> (vidi sliku 4). Potencijali konektora su takvi
   da vlada napon koji struju pokušava protjerati u suprotnom smjeru od onog u kojem dioda vodi. Zbog toga se u tom
   slučaju dioda ponaša kao beskonačan otpor i struja ne teče.
</p>

<figure class="mlFigure">
<img src="@#file#(dioda3.svg)" alt="Poluvodička dioda: nepropusno polarizirana."><br>
<figcaption><span class="mlFigKey">Slika 4.</span> Nepropusno polarizirana poluvodička dioda.</figcaption>
</figure>

<p>Serijski spoj otpornika i ovako polarizirane diode prikazan je na slici u nastavku.</p>

<figure class="mlFigure">
<img src="@#file#(shema-dioda2.png)" alt="Poluvodička dioda: nepropusno polarizirana.">
<figcaption><span class="mlFigKey">Slika 3.</span> Nepropusno polarizirana poluvodička dioda u strujnom krugu.</figcaption>
</figure>

<div class="mlNote"><p>Interaktivna simulacija sheme s prethodne slike dostupna je kao Java aplikacija koju možete pokrenuti
uporabom tehnologije Java WebStart. <a href="@#file#(RunD1.jnlp)">(Aplikacija)</a></p>
</div>

<p>Pokrenite interaktivnu simulaciju ovog strujnog kruga i mijenjajte napon izvora povlačenjem kliznika prikazanog u gornjem lijevom uglu prozora.
   Uvjerite se da će za sve ulazne napone od 0V do 5V dioda trošiti čitav iznos napona čime će napon na otporniku uvijek biti 0V i struja neće teči.
</p>
