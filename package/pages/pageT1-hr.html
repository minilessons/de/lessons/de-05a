<p>
  Pogledajmo sada kako se bipolarni NPN tranzistor može koristiti za ostvarivanje upravljive sklopke.
  Najprije ćemo dati kratko pojašnjenje rada bipolarnog NPN tranzistora a potom prikazati izvedbu
  invertora.
</p>

<h3>Pojednostavljen model bipolarnog NPN tranzistora</h3>

<p>
 Bipolarni NPN tranzistor je elektronički element koji ima tri konektora:
</p>
<ul>
<li><i>kolektor</i> - u našem apstraktnom modelu upravljive sklopke odgovara konektoru <i>A</i>,</li>
<li><i>emiter</i> - u našem apstraktnom modelu upravljive sklopke odgovara konektoru <i>B</i> te</li>
<li><i>baza</i> - u našem apstraktnom modelu upravljive sklopke odgovara konektoru <i>C</i> koji upravlja stanjem sklopke.</li>
</ul>
<p>
  S obzirom na njihova imena, konektore kod bipolarnog tranzistora označavamo slovima C (od engl. <i>collector</i>), 
  E (od engl. <i>emitter</i>) te B (od engl. <i>base</i>). Shodno tome, kod bipolarnog NPN tranzistora zanimljivi su
  nam napon između baze i emitera <i>U<sub>BE</sub></i> te napon između kolektora i emitera <i>U<sub>CE</sub></i>.  
  Simbol bipolarnog NPN tranzistora kao i spomenuti naponi prikazani su na sljedećoj slici. Ujedno je s desne strane
  ponovljen prikaz apstraktne upravljive sklopke o kojoj smo prethodno govorili kako biste uočili analogiju.
</p>

<figure class="mlFigure">
<img src="@#file#(model-npn.png)" alt="NPN-tranzistor: simbol." width="558" height="187"><br>
<figcaption><span class="mlFigKey">Slika 1.</span> Simbol NPN-tranzistora.</figcaption>
</figure>

<p>
  Kako radi bipolarni NPN tranzistor? Krenimo s nekoliko pravila koji se treba pridržavati. Za razliku od apstraktne
  upravljive sklopke kod koje struja može teći u bilo kojem smjeru između kontakata <i>A</i> i <i>B</i>, tijek struje
  kod bipolarnog NPN tranzistora bit će od kolektora prema emiteru. Stoga potencijal kolektora mora biti veći od potencijala
  emitera odnosno napon <i>U<sub>CE</sub></i> mora biti veći ili jednak nuli (što bi se dogodilo u suprotnom slučaju ovdje
  nećemo razmatrati). Jednom kada smo to zadovoljili, treba zapamtiti da je bipolarni tranzistor elektronički element kod
  kojega je <i>maksimalna struja koja može teći kroz kolektor</i> direktno proporcionalna struji koji stvarno teče u bazu. 
</p>
<div class="mlNote"><p>Pogledajte pažljivo sliku 1.a. Uočite: Bipolarni NPN-tranzistor ima tri konektora: bazu, kolektor i emiter. Kod
načina na koji tranzistor koristimo u digitalnim sklopovima, struja izvana teče u bazu i u kolektor, što je strelicama označeno
na slici.</p>
<p>Ako je emiter spojen na masu, bazni strujni krug možemo riješiti neovisno o kolektorskom (kao da taj ne postoji) i potom kad znamo
sve veličine u baznom strujnom krugu, možemo riješiti kolektorski strujni krug.</p></div>
<p>  
  Obje struje (struja baze i struja kolektora) skupljaju se na emiteru i iz njega izlaze van, pa vrijedi:
  \[
    I_E = I_B + I_C.
  \]
  Koeficijent proporcionalnosti kolektorske struje s obzirom na baznu struju nazivamo faktor strujnog pojačanja i označavamo 
  s \(h_{FE}\); vrijedi \(I_{C,\text{max}} = h_{FE} \cdot I_B\). U najjednostavnijem scenariju ovaj broj ćemo smatrati konstantom (u stvarnosti 
  to nije tako, ali sjetite se - zanima nas samo princip rada i najjednostavniji mogući model). U praksi ovaj se broj za različite 
  izvedbe tranzistora kreće od nekoliko desetaka pa do nekoliko stotina.
</p>
<p>
  Drugo važno svojstvo ovog elementa je da će kolektorska struja uvijek biti jednaka maksimalnoj kolektorskoj struji koja je
  određena strujom baze i faktorom strujnog pojačanja ako je to moguće; ako to nije moguće, bit će manja, a brzo ćemo objasniti
  kako se utvrđuje koji je slučaj nastupio. Napon između kolektora i emitera (kada se tranzistor ponaša kao uključena sklopka)
  pri tome neće moći pasti do 0V već do nekog malog napona koji označavamo <i>U<sub>CE,zas</sub></i> 
  (čitamo <i>U<sub>CE</sub></i>-zasićenja) i pravit ćemo se da je taj napon također konstanta proizvedenog elektroničkog elementa.
</p>
<p>
  Konačno, još treba zapamtiti kako baza utječe na rad tranzistora. Pogledajte na slici 2 strujni krug (konturu) koju čine
  izvor \(U_{ul}\), otpornik \(R_b\) i spoj baza-emiter tranzistora. Konceptualno, između baze i emitera nalazi se dioda, a
  takav strujni krug smo već upoznali. Između baze i kolektora postoji
  definiran napon praga (označit ćemo ga <i>U<sub>BE,th</sub></i>) koji je reda veličine 0,7V. Tako dugo dok je napon između 
  baze i emitera manji od napona praga tj. <i>U<sub>BE</sub>&lt;U<sub>BE,th</sub></i>, baza tranzistora predstavlja
  beskonačan otpor pa je struja u baznom strujnom krugu jednaka 0, odnosno ne teče. Povećavamo li polako napon izvora \(U_{ul}\),
  rast će i napon <i>U<sub>BE</sub></i> (koji će tako dugo dok je \(U_{ul}\) manji od <i>U<sub>BE,th</sub></i> biti upravo jednak naponu
  \(U_{ul}\)). Jednom kada napon između baze i emitera dosegne napon praga, više se neće
  povećavati (jer bi struja rapidno porasla i uništila element) već će se ostatak napona izvora trošiti na serijski spojenom baznom
  otporniku \(R_b\). Time će automatski biti određena i bazna struja koja teče u tom strujnom krugu.
</p>

<p>
  Pogledajmo sada jedan konkretan strujni krug u kojem je spojen bipolarni NPN tranzistor. Slika je prikazana u nastavku.
</p>

<figure class="mlFigure">
<img src="@#file#(npn-strujni-krug-1.png)" alt="NPN-tranzistor: strujni krug." width="391" height="205"><br>
<figcaption><span class="mlFigKey">Slika 2.</span> NPN-tranzistor u strujnom krugu.</figcaption>
</figure>

<p>
  Uočite postojanje dvaju strujnih krugova. Bazni strujni krug čine izvor \(U_{ul}\), otpornik \(R_B\) te spoj baza-emiter.
  Kolektorski strujni krug čine izvor \(U_{cc}\), otpornik \(R_C\) te spoj kolektor-emiter. Pretpostavimo da za komponente koje
  su korištene u prikazanoj shemi vrijedi: \(R_C = 330 \Omega\), \(R_B = 5600 \Omega\), \(U_{BE,th} = 0,7 V\), \(U_{CE,zas} = 0,2V\) 
  te \(h_{FE} = 50\).
</p>

<h3>Strujni krug baze</h3>

<p>
  Analizirajmo sada strujni krug baze. Sjetite se što smo prethodno rekli da vrijedi: u prikazanom strujnom krugu gdje je emiter spojen na masu,
  strujni krug baze može se analizirati neovisno o ostatku sklopa (kolektorski strujni krug nije bitan). Vrijedi:
  \[
    U_{ul} = U_{R_B} + U_{BE} = I_B \cdot R_B + U_{BE} 
  \]
</p>

<p>
  <b>Slučaj 1.</b> Pretpostavimo da je napon \(U_{ul} = 0V\). Odredimo sve vrijednosti u baznom krugu.
</p>
<p>
  Budući da je ulazni napon manji od napona praga \(U_{BE,th}\), nema bazne struje. To znači da je \(U_{BE}=U_{ul}\) što je u ovom slučaju 0V,
  \(I_B = 0A\), \(U_{R_B} = 0V\).
</p>

<p>
  <b>Slučaj 2.</b> Pretpostavimo da je napon \(U_{ul} = 0,4V\). Odredimo sve vrijednosti u baznom krugu.
</p>
<p>
  Budući da je ulazni napon i dalje manji od napona praga \(U_{BE,th}\), nema bazne struje. To znači da je \(U_{BE}=U_{ul}\) što je u ovom slučaju 0,4V,
  \(I_B = 0A\), \(U_{R_B} = 0V\). Uočite, baš kao kod strujnog kruga s otpornikom i diodom, nalazimo se u području u kojem se sav napon izvora troši na
  spoju (diodi) baza-emiter.
</p>

<p>
  Slučajevi 1 i 2 predstavljaju situaciju u kojoj za tranzistor kažemo da je u <i>zapiranju</i> (isključen).
</p>

<p>
  <b>Slučaj 3.</b> Pretpostavimo da je napon \(U_{ul} = 1V\). Odredimo sve vrijednosti u baznom krugu.
</p>
<p>
  Kako je sada ulazni napon veći od napona praga \(U_{BE,th}\), na spoju baze i emitera vladat će upravo napon praga, tj. \(U_{BE}=U_{BE,th}=0,7V\). Preostali
  napon trošit će se na baznom otporniku:
  \[
    U_{R_B} = U_{ul} - U_{BE} = 1V - 0,7V = 0,3V.
  \]
   Stoga je struja baze odredena strujom kroz bazni otpornik prema Ohmovom zakonu:
   \[
     I_B = \frac{U_{R_B}}{R_B} = \frac{0,3V}{5600 \Omega} = 53,57 {\mu}A
   \]
  čime je maksimalna kolektorska struja \[
    I_{C,max} = I_B \cdot h_{FE} = 53,57 {\mu}A \cdot 50 = 2,68 mA.
   \]
</p>

<p>
  <b>Slučaj 4.</b> Pretpostavimo da je napon \(U_{ul} = 3V\). Odredimo sve vrijednosti u baznom krugu.
</p>
<p>
  Kako je i sada ulazni napon veći od napona praga \(U_{BE,th}\), na spoju baze i emitera vladat će upravo napon praga, tj. \(U_{BE}=U_{BE,th}=0,7V\). Preostali
  napon trošit će se na baznom otporniku:
  \[
    U_{R_B} = U_{ul} - U_{BE} = 3V - 0,7V = 2,3V.
  \]
   Stoga je struja baze određena strujom kroz bazni otpornik prema Ohmovom zakonu:
   \[
     I_B = \frac{U_{R_B}}{R_B} = \frac{2,3V}{5600 \Omega} = 410,7 {\mu}A
   \]
  čime je maksimalna kolektorska struja \[
    I_{C,max} = I_B \cdot h_{FE} = 410,7 {\mu}A \cdot 50 = 20,54 mA.
   \]
</p>

<h3>Strujni krug kolektora</h3>

<p>
  Za strujni krug kolektora možemo postaviti sljedeću jednadžbu.
  \[
    U_{cc} = U_{R_C} + U_{CE} = I_C \cdot R_C + U_{CE}.
  \]
  Vidimo dakle da će se napon izvora \(U_{cc}\) razdijeliti na pad napona na kolektorskom otporniku te na napon između kolektora i emitera. No kako odrediti kakva
  će biti ta razdioba? Sjetite se <i>(a)</i> što smo rekli o kolektorskoj struji: ona će biti uvijek jednaka maksimalnoj (određenoj strujom baze) kad god je to moguće te
  <i>(b)</i> napon između kolektora i emitera ne može pasti na manje od napona zasićenja. Ako se pitate zašto bi taj napon padao, uočite sljedeće: suma padova napona na otporniku i spoju kolektor-emiter
  je konstantna (jednaka je naponu izvora \(U_{cc}\)); prema Ohmovom zakonu, porastom struje kroz otpornik \(R_C\) na njemu raste i pad napona ali tada mora doći do jednakog 
  smanjenja napona na spoju kolektor-emiter (jer im je suma konstantna).
</p>
<p>
  Pogledajmo sada kakva će biti situacija u strujnom krugu kolektora za prethodna četiri slučaja koja smo analizirali za bazni strujni krug.
</p>

<p>
  <b>Slučaj 1.</b> Zaključili smo da je struja baze \(I_B = 0A\). Slijedi da je maksimalna kolektorska struja jednaka \(I_B = I_C \cdot h_{FE} = 0A\), pa je to ujedno i stvarni 
  iznos kolektorske struje. Tada je pad napona na kolektorskom otporniku \(U_{R_C} = I_C \cdot R_C = 0V\) pa je napon između kolektora i emitera jednak:
  \( U_{CE} = U_{cc} - U_{R_C} = 5V - 0V = 5V\). Vrlo jednostavno!
</p>

<p>
  <b>Slučaj 2.</b> I ovdje smo zaključili da je struja baze \(I_B = 0A\), pa su sve vrijednosti u kolektorskom krugu jednake onima iz prethodnog slučaja: \(I_C = 0A\), \(U_{R_C} = 0V\),
  \(U_{CE} = U_{cc} = 5V\). 
</p>

<p>
  <b>Slučaj 3.</b> Zaključili smo da je struja baze \(I_B = 53,57 {\mu}A\) odnosno da je maksimalna kolektorska struja jednaka \(I_{C,max} = 2,68 mA\). Pretpostavimo li da je kolektorska
  struja jednaka toj struji, tj. da je \( I_C = I_{C,max} = 2,68 mA\), pad napona na otporniku \(R_C\) bit će prema Ohmovom zakonu \(U_{R_C} = I_C \cdot R_C = 2,68 mA \cdot 330 \Omega = 0,884V\).
  Napon između kolektora i emitera tada je \(U_{CE} = U_{cc} - U_{R_C} = 5V - 0,884V = 4,1156V\). S obzirom da je taj napon veći od napona zasićenja, situacija u strujnom krugu će doista
  i biti takva.
</p>

<p>
  <b>Slučaj 4.</b> Zaključili smo da je struja baze \(I_B = 410,7 {\mu}A\) odnosno da je maksimalna kolektorska struja jednaka \(I_{C,max} = 20,54 mA\). Pretpostavimo li da je kolektorska
  struja jednaka toj struji, tj. da je \( I_C = I_{C,max} = 20,54 mA\), pad napona na otporniku \(R_C\) bit će prema Ohmovom zakonu \(U_{R_C} = I_C \cdot R_C = 20,54 mA \cdot 330 \Omega = 6,7782V\).
  Napon između kolektora i emitera tada bi bio \(U_{CE} = U_{cc} - U_{R_C} = 5V - 6,7782V = -1,7782V\), što je ne samo manje od napona zasićenja već je i negativno pa je fizikalno nemoguće. Prva
  indikacija da nešto ne štima već je bila činjenica da smo dobili da je napon na otporniku \(R_C\) veći od napona napajanja (5V). Što ovaj rezultat znači? Znači da smo pogrešno pretpostavili da
  će u ovoj situaciji kolektorska struja i dalje biti jednaka maksimalnoj kolektorskoj struji: uz ovakvu struju baze i konfiguraciju sklopa to naprosto više nije moguće. Ono što će se dogoditi jest
  da će se napon između kolektora i emitera stabilizirati na vrijednosti koja odgovara naponu zasićenja, pa imamo \(U_{CE} = U_{CE,zas} = 0,2V\). No tada se ostatak napona \(U_{cc}\) troši na
  kolektorskom otporniku pa vrijedi \(U_{R_C} = U_{cc} - U_{CE} = 5V - 0,2V = 4,8V\). Kolektorsku struju sada možemo izračunati prema Ohmovom zakonu iz omjera napona i otpora otpornika \(R_C\) pa imamo:
  \(I_C = \frac{U_{R_C}}{R_C} = \frac{4,8V}{330\Omega} = 14,55 mA\).
</p>

<p>
  <b>Što možemo zaključiti?</b> Krenimo od situacije u kojoj smo imali u baznom krugu izvor napona 0V. Tranzistor je bio u zapiranju, nije bilo struje baze pa time niti struje kolektora. Napon
  \(U_{CE}\) stoga je bio jednak naponu napajanja u kolektorskom krugu \(U_{cc}\). Porastom napona u baznom krugu jedno vrijeme se nije događalo ništa osim porasta napona na spoju baza-emiter.
  No tako dugo dok taj napon nije dosegao napon praga, struja baze je i dalje bila 0A a napon kolektor-emiter bio je jednak naponu napajanja.
</p>
<p>
  Daljnjim porastom napona izvora u krugu baze na spoju baza-emiter napon je ostao fiksan i jednak naponu praga, a ostatak napona trošio se na baznom otporniku; uz svako povećanje napona izvora
  u krugu baze dolazi do upravo tolikog povećanja napona na baznom otporniku, a time i do porasta bazne struje. Kako počinje rasti struja baze, tako počinje rasti i struja kolektora, a time i pad
  napona na kolektorskom otporniku. Kako je zbroj pada napona na kolektorskom otporniku i napona između kolektora i emitera konstantan (i jednak \(U_{cc}\)), slijedi da napon između kolektora i emitera
  sve više i više pada; veća struja baze, još veća struja kolektora, još manji napon između kolektora i emitera. Kada tranzistor radi u ovom području, kažemo da je u <i>normalnom aktivnom području</i>. 
</p>

<p>
  Konačno, daljnjim povećanjem napona izvora u baznom krugu doći ćemo do struje baze koja će generirati upravo toliku struju kolektora uz koju će napon između kolektora i emitera pasti na svoj
  minimum - na napon zasićenja. Daljnjim povećanjem napona izvora u baznom krugu struja baze će i dalje rasti, ali taj porast kolektorska struja više neće slijediti: ona će ostati zaglavljena upravo
  na onoj struji uz koju je pad napona na otporniku \(R_C\) bio takav da je napon između kolektora i emitera pao na napon zasićenja. Kada tranzistor uđe u ovaj režim rada, kažemo da je ušao u
  <i>zasićenje</i>.
</p>

<div class="mlImportant"><p>Tranzistor je nelinearni element. Može se nalaziti u jednom od tri stanja:</p>
<ul class="mlSpread">
<li><span class="li-inline-definition">Zapiranje.</span> To je stanje tranzistora u kojem na spoju baza-emiter napon nije dosegnuo napon praga pa ne teče bazna struja. Zbog toga 
spoj kolektor-emiter predstavlja beskonačan otpor (kažemo da je tranzistor <em>isključen</em>).</li>
<li><span class="li-inline-definition">Normalno aktivno područje.</span> To je stanje tranzistora u kojem je na spoju baza-emiter napon jednak naponu praga i teče bazna struja uz
koju je stvarna kolektorska struja jednaka \(h_{FE} \cdot I_B\) a napon kolektor-emiter je veći od napona zasićenja.</li>
<li><span class="li-inline-definition">Zasićenje.</span> To je stanje tranzistora u kojem je na spoju baza-emiter napon jednak naponu praga i teče bazna struja uz
koju je stvarna kolektorska struja manja od maksimalne moguće određene izrazom \(h_{FE} \cdot I_B\); napon kolektor-emiter pao je na minimalnu vrijednost i jednak je naponu zasićenju.</li>
</ul>
<p>Pri analizi strujnog kruga potrebno je izračunati kakva je bazna struja, pa ako je 0A, tranzistor je u zapiranju a ako je veća od nule, treba provjeriti može li kolektorskim strujnim krugom
teči struja koja je određena s \(h_{FE} \cdot I_B\) a da pri tome napon kolektor-emiter ne bude manji od napona zasićenja. Ako može, onda ta struja i teče i tranzistor je u normalnom aktivnom
području; ako ne može, onda je tranzistor u zasićenju i napon kolektor-emiter jednak je naponu zasićenja; razlika se troši na kolektorskom otporu i prema Ohmovom zakonu određuje struju u
kolektorskom strujnom krugu.</p>
</div>

<h3>Interaktivna demonstracija</h3>

<p>
  Kako biste mogli uživo isprobati ponašanje tranzistora u okruženju kakvo smo upravo opisali, u nastavku je pripremljena interaktivna demonstracija koja Vam omogućava da mijenjate
  napon izvora u strujnom krugu baze od 0V do 5V. Izgled simulacije prikazan je na slici 3.</p>
  
<figure class="mlFigure">
<img src="@#file#(shema-npn.png)" alt="NPN-tranzistor u strujnom krugu.">
<figcaption><span class="mlFigKey">Slika 3.</span> NPN-tranzistor u strujnom krugu.</figcaption>
</figure>

<p>Krenite s polaganim povećanjem ovog napona i uvjerite se da se sve veličine mijenjaju upravo ovako kako smo opisali. Potrebno je
  napomenuti da se ovdje radi o pojednostavljenom modelu ponašanja koji je dovoljan da bismo shvatili kako se ovaj tip tranzistora koristi kao sklopka. Puno detaljnije učit ćete
  o ovome na drugim kolegijima.
</p>

<div class="mlNote"><p>Interaktivna simulacija sheme s prethodne slike dostupna je kao Java aplikacija koju možete pokrenuti
uporabom tehnologije Java WebStart. <a href="@#file#(RunBT1.jnlp)">(Aplikacija)</a></p>
</div>

<h3>Funkcija sklopa</h3>

<p>
  Sada kada razumijete osnovna načela rada bipolarnog NPN tranzistora (s ponašajnog aspekta), pogledajmo još jednom strujni krug ilustriran u prethodnoj demonstraciji. Pretpostavimo
  da izvor \(U_{ul}\) predstavlja ulaz sklopa i da taj napon ne generira fiksna baterija već izlaz nekog drugog prethodno spojenog sklopa. Pretpostavite također da naš sklop ima jedan
  izlaz: neka je izlazni napon jednak naponu koji vlada između kolektora i emitera. Provjerite sada dvije tipične situacije opisane u nastavku. 
</p>
<p>
  <b>Prvi slučaj.</b> Podesite da je ulazni napon jednak 0V (niska naponska razina). Koliki je tada izlazni napon?
</p>
<p>
  <b>Drugi slučaj.</b> Podesite da je ulazni napon jednak 5V (visoka naponska razina). Koliki je tada izlazni napon?
</p>

<p>
  Ako ste dobro radili, dobili ste vrijednosti koje prikazuje <i>tablica naponskih kombinacija</i> prikazana u nastavku.
</p>


<div class="mlTableCaption"><span class="mlTblKey">Tablica 1.</span> Tablica naponskih kombinacija.</div>
<table class="mlTableCC mlTableVR2">
 <thead>
  <tr><th><i>U<sub>ul</sub></i></th><th><i>U<sub>izl</sub></i></th></tr>
 </thead>
 <tbody>
  <tr><td>0V</td><td>5V</td></tr>
  <tr><td>5V</td><td>0.2V</td></tr>
 </tbody>
</table>

<p>
  U svijetu digitalnih uređaja, iako se u stvarnosti razmjenjuju analogne veličine, njihove iznose uvijek tjeramo u krajnosti koje možemo lagano pretvoriti u binarne vrijednosti: 
  0 ili 1. Neka vrijedi sljedeći dogovor: niska naponska razina predstavljat će logičku vrijednost 0 (<i>laž</i>) a visoka naponska razina logičku vrijednost 1 (<i>istinu</i>). 
  U tom slučaju, čitav sklop možemo promatrati kao sklop koji obavlja određenu logičku funkciju. Sklop koji smo istražili ima jedan ulaz i jedan izlaz i simbolički je prikazan 
  u nastavku.
</p>

<figure class="mlFigure">
<img src="@#file#(inv-model.png)" alt="Model našeg sklopa." width="190" height="72"><br>
<figcaption><span class="mlFigKey">Slika 4.</span> Model našeg sklopa.</figcaption>
</figure>

<p>
  Neka ulazni napon (odnosno njegova pretvorba u logičku vrijednost u skladu s prethodnim dogovorom o niskim i visokim razinama) predstavlja ulaz <i>A</i>. Neka izlazni napon odnosno njegova
  pretvorba u logičku vrijednost odgovara izlazu <i>f</i>. Zanima nas koju funkciju obavlja elektronički sklop koji smo upravo konstruirali. U tu svrhu tablicu naponskih kombinacija
  pretvorit ćemo u <i>tablicu istinitosti</i>: to je tablica koja umjesto napona sadrži logičke vrijednosti koje su dobivene interpretacijom naponskih vrijednosti u logičke vrijednosti.
</p>

<div class="mlTableCaption"><span class="mlTblKey">Tablica 2.</span> Dobivena tablica istinitosti.</div>
<table class="mlTableCC mlTableVR2">
 <thead>
  <tr><th><i>A</i></th><th><i>f</i></th></tr>
 </thead>
 <tbody>
  <tr><td>0</td><td>1</td></tr>
  <tr><td>1</td><td>0</td></tr>
 </tbody>
</table>

<p>
  Prvi redak tablice nam kaže sljedeće: kada je ulaz bio logička 0, izlaz je logička 1.
  Drugi redak tablice nam kaže sljedeće: kada je ulaz bio logička 1, izlaz je logička 0.
  Slijedi da je izlaz uvijek negacija ulaza: opisani sklop obavlja dakle <i>logičku funkciju NE</i>. Stoga taj sklop još nazivamo i <i>invertorom</i> jer okreće logičke vrijednosti.
  Postoje dva uobičajena simbola koja se koriste za prikaz invertora u shemama digitalnih sklopova
  i oba su prikazana na slici u nastavku. 
</p>

<figure class="mlFigure">
<img src="@#file#(inv-simbol.png)" alt="Invertor: simbol." width="330" height="62"><br>
<figcaption><span class="mlFigKey">Slika 5.</span> Simbol invertora.</figcaption>
</figure>

<h3>Primjer NPN-tranzistora</h3>

<p>
  Pogledajmo za primjer bipolarni NPN tranzistor oznake 2N3904. Više informacija o tom tranzistoru
  možete pronaći u <a href="http://www.fairchildsemi.com/ds/2N/2N3904.pdf" target="_blank">njegovoj dokumentaciji</a>. Slika ovog tranzistora u plastičnom kućištu prikazana je u nastavku.
</p>

<figure class="mlFigure">
<img src="@#file#(2N3904.jpg)" alt="Tranzistor 2N3904" width="300" height="225"><br>
<figcaption><span class="mlFigKey">Slika 6.</span> Tranzistor 2N3904.</figcaption>
</figure>

<p>
  Raspored pinova (koji pin je baza, koji kolektor a koji emiter) prikazan je na
  sljedećoj slici.
</p>

<figure class="mlFigure">
<img src="@#file#(2N3904-pin-layout.png)" alt="Tranzistor 2N3904: raspored pinova." width="276" height="224"><br>
<figcaption><span class="mlFigKey">Slika 7.</span> Tranzistor 2N3904: raspored pinova.</figcaption>
</figure>

