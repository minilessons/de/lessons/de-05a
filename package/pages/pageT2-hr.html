<p>
  Drugi tip tranzistora koji se u praksi koriste u digitalnim sklopovima za ostvarivanje sklopki
  su MOSFET-i. Način proizvodnje i rada ove vrste tranzistora uvelike se razlikuje od bipolarnih
  tranzistora i mi ovdje nećemo ići u detalje. Prikazat ćemo samo najosnovnije što nam je potrebno
  da bismo razumjeli kako uporabom ovog tranzistora napraviti upravljivu sklopku, a potom i invertor.
</p>

<h3>Pojednostavljen model n-kanalnog MOSFET-a</h3>

<p>
 n-kanalni MOSFET je elektronički element koji ima tri konektora:
</p>
<ul>
<li><span class="li-inline-definition">odvod</span> - u našem apstraktnom modelu upravljive sklopke odgovara konektoru <i>A</i>,</li>
<li><span class="li-inline-definition">uvod</span> - u našem apstraktnom modelu upravljive sklopke odgovara konektoru <i>B</i> te</li>
<li><span class="li-inline-definition">upravljačka elektroda</span> - u našem apstraktnom modelu upravljive sklopke odgovara konektoru <i>C</i> koji upravlja stanjem sklopke.</li>
</ul>
<p>
  S obzirom na njihova imena, konektore kod MOSFET tranzistora označavamo slovima D za odvod (od engl. <i>drain</i>), 
  S za uvod (od engl. <i>source</i>) te G za upravljačku elektrodu (od engl. <i>gate</i>). Shodno tome, kod ove vrste 
  tranzistora zanimljivi su nam napon između upravljačke elektrode i uvoda <i>U<sub>GS</sub></i> te napon između odvoda i uvoda <i>U<sub>DS</sub></i>.  
  Simbol n-kanalnog MOSFET-a kao i spomenuti naponi prikazani su na sljedećoj slici. Ujedno je s desne strane
  ponovljen prikaz apstraktne upravljive sklopke o kojoj smo prethodno govorili kako biste uočili analogiju.
</p>

<figure class="mlFigure">
<img src="@#file#(model-nmosfet.png)" alt="Simbol n-kanalnog MOSFET-a." width="564" height="188"><br>
<figcaption><span class="mlFigKey">Slika 1.</span> Simbol n-kanalnog MOSFET-a.</figcaption>
</figure>

<p>
  Rad n-kanalnog MOSFET-a obradit ćemo puno površnije no što je to bio slučaj s bipolarnim NPN-tranzistorom.
  Zadovoljit ćemo se pri tome sa sljedećime. Za razliku od bipolarnog NPN tranzistora koji je upravljan strujom baze,
  MOSFET-i su čisti naponski elementi. Upravljačka elektroda za strujni krug predstavlja beskonačan otpor pa tim putem
  struja ne teče. Kao i bipolarni tranzistor, n-kanalni MOSFET ima napon praga (označit ćemo ga s \(U_{GS,th}\)): to je 
  minimalno potreban napon između upravljačke elektrode <i>G</i> i uvoda <i>S</i> ispod kojeg je MOSFET isključen i 
  predstavlja vrlo velik otpor između elektroda <i>D</i> i <i>S</i>. Ako napon \(U_{GS,th}\) postane veći od napona praga,
  tranzistor se uključuje i struja koja može proticati od odvoda prema uvodu postaje sve veća i veća. Međutim, za razliku
  od bipolarnog tranzistora kod kojeg je napon između baze i emitera, jednom kada se tranzistor uključio, ostajao praktički
  konstantan, napon između upravljačke elektrode i uvoda možemo neometano podizati iznad napona praga -- kako je upravljačka
  elektroda beskonačan otpor za struju, struja tim putem i dalje neće teči.
</p>
<p>
  Točne izraze prema kojima se računaju struje i naponi kod MOSFET-a nećemo navoditi - to ćete moći naučiti na drugim kolegijima FER-a.
  Ono što je važno za razumijevanje n-kanalnog MOSFET-a kada ga se koristi kao sklopku jest da je njegovo ponašanje na ovoj visokoj
  ponašajnoj razini ekvivalentno ponašanju bipolarnog NPN tranzistora, uz napomenu da prilikom upravljanja ovdje gledamo samo 
  iznos napona između upravljačke elektrode i uvoda.
</p>

<h3>Interaktivna demonstracija</h3>

<p>
  Kako biste uživo mogli isprobati kako se n-kanalni MOSFET ponaša u jednostavnom strujnom krugu, pripremljena je interaktivna demonstracija
  koja Vam je dostupna u nastavku.
</p>

<figure class="mlFigure">
<img src="@#file#(shema-nmosfet.png)" alt="n-kanalni MOSFET u strujnom krugu." width="620" height="530">
<figcaption><span class="mlFigKey">Slika 2.</span> n-kanalni MOSFET u strujnom krugu.</figcaption>
</figure>

<p>Mijenjajte polako ulazni napon (koji je ujedno i napon \(U_{GS}\)). Demonstracija simulira rad
  MOSFET-a čiji je napon praga 2V. Uočite kako zbog toga u strujnom krugu odvod-uvod struja neće teči sve do trenutka kada napon \(U_{GS}\) ne
  dosegne taj napon praga. Od tog trenutka na dalje povećanje \(U_{GS}\)-a povećava struju kroz odvod, ali time i pad napona na otporniku \(R_D\)
  čime pada izlazni napon \(U_{DS}\) - ovaj trend može se nastaviti samo do određenog minimalnog iznosa \(U_{DS}\) nakon kojeg se struja više ne 
  mijenja i tranzistor je u zasićenju.
</p>

<div class="mlNote"><p>Interaktivna simulacija sheme s prethodne slike dostupna je kao Java aplikacija koju možete pokrenuti
uporabom tehnologije Java WebStart. <a href="@#file#(RunMT1.jnlp)">(Aplikacija)</a></p>
</div>

<h3>Funkcija sklopa</h3>

<p>
  Sada kada razumijete osnovna načela rada n-kanalnog MOSFET-a (s ponašajnog aspekta), pogledajmo još jednom strujni krug ilustriran u prethodnoj demonstraciji. Pretpostavimo
  da izvor \(U_{ul}\) predstavlja ulaz sklopa i da taj napon ne generira fiksna baterija već izlaz nekog drugog prethodno spojenog sklopa. Pretpostavite također da naš sklop ima jedan
  izlaz: neka je izlazni napon jednak naponu koji vlada između odvoda i uvoda. Provjerite sada dvije tipične situacije opisane u nastavku. 
</p>
<p>
  <b>Prvi slučaj.</b> Podesite da je ulazni napon jednak 0V (niska naponska razina). Koliki je tada izlazni napon?
</p>
<p>
  <b>Drugi slučaj.</b> Podesite da je ulazni napon jednak 5V (visoka naponska razina). Koliki je tada izlazni napon?
</p>

<p>
  Ako ste dobro radili, dobili ste vrijednosti koje prikazuje <i>tablica naponskih kombinacija</i> prikazana u nastavku.
</p>

<div class="mlTableCaption"><span class="mlTblKey">Tablica 1.</span> Tablica naponskih kombinacija.</div>
<table class="mlTableCC mlTableVR2">
 <thead>
  <tr><th><i>U<sub>ul</sub></i></th><th><i>U<sub>izl</sub></i></th></tr>
 </thead>
 <tbody>
  <tr><td>0V</td><td>5V</td></tr>
  <tr><td>5V</td><td>0.08V</td></tr>
 </tbody>
</table>

<p>
  Neka vrijedi sljedeći dogovor: niska naponska razina predstavljat će logičku vrijednost 0 (<i>laž</i>) a visoka naponska razina logičku vrijednost 1 (<i>istinu</i>). 
  Čitav sklop sada možemo promatrati kao sklop koji obavlja određenu logičku funkciju. Sklop koji smo istražili ima jedan ulaz i jedan izlaz i simbolički je prikazan 
  u nastavku.
</p>

<figure class="mlFigure">
<img src="@#file#(inv-model.png)" alt="Model našeg sklopa." width="190" height="72"><br>
<figcaption><span class="mlFigKey">Slika 3.</span> Model našeg sklopa.</figcaption>
</figure>

<p>
  Neka ulazni napon (odnosno njegova pretvorba u logičku vrijednost u skladu s prethodnim dogovorom o niskim i visokim razinama) predstavlja ulaz <i>A</i>. Neka izlazni napon odnosno njegova
  pretvorba u logičku vrijednost odgovara izlazu <i>f</i>. Zanima nas koju funkciju obavlja eketronički sklop koji smo upravo konstruirali. U tu svrhu tablicu naponskih kombinacija
  pretvorit ćemo u <i>tablicu istinitosti</i>: to je tablica koja umjesto napona sadrži logičke vrijednosti koje su dobivene interpretacijom naponskih vrijednosti u logičke vrijednosti.
</p>

<div class="mlTableCaption"><span class="mlTblKey">Tablica 2.</span> Dobivena tablica istinitosti.</div>
<table class="mlTableCC mlTableVR2">
 <thead>
  <tr><th><i>A</i></th><th><i>f</i></th></tr>
 </thead>
 <tbody>
  <tr><td>0</td><td>1</td></tr>
  <tr><td>1</td><td>0</td></tr>
 </tbody>
</table>

<p>
  Prvi redak tablice nam kaže sljedeće: kada je ulaz bio logička 0, izlaz je logička 1.
  Drugi redak tablice nam kaže sljedeće: kada je ulaz bio logička 1, izlaz je logička 0.
  Slijedi da je izlaz uvijek negacija ulaza: opisani sklop obavlja dakle <i>logičku funkciju NE</i> čime smo pokazali još jednu izvedbu invertora.
</p>

<h3>Primjer n-kanalnog MOSFET-a</h3>

<p>
  Kao primjer n-kanalnog MOSFET-a možemo pogledati tranzistor oznake 2N7000. Više informacija o tom tranzistoru
  možete pronaći u njegovoj dokumentaciji <a href="http://pdf.seekdatasheet.com/2N7/2N7000.pdf" target="_blank">ovdje</a> ili 
  <a href="http://www.onsemi.com/pub_link/Collateral/2N7000-D.PDF" target="_blank">ovdje</a>. Slika ovog tranzistora u plastičnom kućištu prikazana je u nastavku.
</p>

<figure class="mlFigure">
<img src="@#file#(2N7000.jpg)" alt="Tranzistor 2N7000" width="320" height="320"><br>
<figcaption><span class="mlFigKey">Slika 4.</span> Tranzistor 2N7000.</figcaption>
</figure>

<p>
  Raspored pinova (koji je pin upravljačka elektroda, koji odvod a koji uvod) prikazan je na
  sljedećoj slici. Uočite da je pri tome dan simbol koji je puno informativniji od jednostavnog simbola koji smo mi koristili - i to je OK; mi ćemo i dalje na kolegiju koristiti
  pojednostavljeni simbol s kojim smo započeli razmatranja.
</p>

<figure class="mlFigure">
<img src="@#file#(2N7000-pin-layout.png)" alt="Tranzistor 2N7000: raspored pinova." width="495" height="231"><br>
<figcaption><span class="mlFigKey">Slika 5.</span> Tranzistor 2N7000: raspored pinova.</figcaption>
</figure>
